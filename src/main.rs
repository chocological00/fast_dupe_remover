use std::{
	collections::HashSet,
	fs::{self, File},
	io::{self, BufWriter},
	mem,
	path::{Path, PathBuf},
	sync::{
		mpsc::{self, Sender},
		Arc,
		Mutex
	},
	thread,
	vec::IntoIter
};

use blake3::{Hash, Hasher};
use dashmap::DashMap;
use tracing::*;
use tracing_subscriber::{
	filter::{EnvFilter, LevelFilter},
	fmt
};
use walkdir::WalkDir;

struct ProcessedMsg;

fn main()
{
	// logger init
	let filter = EnvFilter::builder().with_default_directive(LevelFilter::INFO.into()).parse_lossy("");
	let format = fmt::format().with_target(false).without_time().compact();
	tracing_subscriber::fmt().event_format(format).with_env_filter(filter).init();

	// read path, sanity check
	let path = loop
	{
		let mut s = String::new();
		info!("Please enter the path below:");
		io::stdin().read_line(&mut s).unwrap();
		let p = Path::new(&s.trim()).to_owned();

		// sanity check
		let m = if let Ok(m) = fs::metadata(&p)
		{
			m
		}
		else
		{
			warn!("Path {:?} is not a directory!", &p);
			continue;
		};
		if !m.is_dir()
		{
			continue;
		}

		// user confirmation
		s.clear();
		info!("Parsed path is {:?}", &p);
		info!("Is above information correct? Press y to continue:");
		io::stdin().read_line(&mut s).unwrap();
		if !s.trim().eq_ignore_ascii_case("y")
		{
			continue;
		}
		break p;
	};

	// walk directory
	info!("Reading directory: {:?}", &path);
	let mut errs = 0;
	let entries = WalkDir::new(&path)
		.into_iter()
		.filter_map(|e| {
			// map/report error
			let e = if let Ok(e) = e
			{
				e
			}
			else
			{
				errs += 1;
				warn!("DirEntry read error: {:?}", e);
				return None;
			};

			// filter files only
			if !e.file_type().is_file()
			{
				return None;
			}

			Some(e.into_path())
		})
		.collect::<Vec<_>>();
	if errs != 0
	{
		let mut s = String::new();
		warn!("{} entries could not be read", errs);
		info!("Press y to ignore and proceed:");
		io::stdin().read_line(&mut s).unwrap();
		if !s.trim().eq_ignore_ascii_case("y")
		{
			error!("Aborting...");
			thread::sleep(std::time::Duration::from_secs(5));
			std::process::exit(1);
		}
	}
	let total = entries.len();
	info!("{} entires found.", total);

	// multithreaded hashing, convert to inverse map of <Hash, Vec<(Path, Size, Extension)>> for file with each extension
	info!("Hashing files to find duplicates...");
	let files = Arc::new(Mutex::new(entries.into_iter()));
	let map: Arc<DashMap<Hash, Vec<(PathBuf, u64, Option<String>)>>> = Arc::new(DashMap::new());
	let (tx, rx) = mpsc::channel::<ProcessedMsg>();

	let mut handles = Vec::new();
	for i in 0..num_cpus::get()
	{
		info!("Running in CPU{}", i);
		let tx = tx.clone();
		let files = Arc::clone(&files);
		let map = Arc::clone(&map);
		let h = thread::spawn(move || hash_and_insert(tx, files, map));
		handles.push(h);
	}
	let mut processed = 0;
	let process_granularity = if total > 1000 { 100 } else { 10 };
	loop 
	{
		// sleep until msg
		let _ = rx.recv().unwrap();
		
		processed += 1;
		if processed % (total / process_granularity) == 0
		{
			let progress = processed as f64 * 100.0 / total as f64;
			info!("{}%\tProcessed", progress.round());
		}

		if processed == total { break }
	}

	// reap zombies
	for h in handles.into_iter()
	{
		h.join().unwrap();
	}
	info!("File hashing done!");

	// iterate through map and delete files
	let map = Arc::try_unwrap(map).unwrap().into_iter();
	for (_, v) in map
	{
		if v.len() <= 1
		{
			continue;
		}

		// if dupe exists
		// generate extension set
		let exts = v.iter().fold(HashSet::new(), |mut acc, (_, _, ext)| 
		{
			acc.insert(ext.as_ref());
			acc
		});

		// for each extension
		for ext in exts
		{
			// get everything with the current extension
			let dupes = v.iter().filter_map(|(_, s, e)| if ext == e.as_ref() { Some(*s) } else { None });
			// generate size set
			let sizes = dupes.fold(HashSet::new(), |mut acc, size|
			{
				acc.insert(size);
				acc
			});
			for size in sizes
			{
				// get everything with current extension and current size
				let real_dupes = v.iter().filter_map(|(p, s, e)| if ext == e.as_ref() && *s == size { Some(p) } else { None }).collect::<Vec<_>>();
				if real_dupes.len() <= 1
				{
					continue;
				}
				println!();
				info!("Found duplicates of\t{:?}", real_dupes[0]);
				for dupe in &real_dupes[1..]
				{
					info!("Deleting duplicate\t{:?}...", dupe);
					if let Err(e) = fs::remove_file(dupe)
					{
						error!("Could not remove {:?}:\n{:?}",dupe, e);
					}
				}
			}
		}
	}

	// now re-iterate the folder and find name dupes, rename each one of them
}

fn hash_and_insert(notifier: Sender<ProcessedMsg>, files: Arc<Mutex<IntoIter<PathBuf>>>, map: Arc<DashMap<Hash, Vec<(PathBuf, u64, Option<String>)>>>)
{
	loop
	{
		// get next path from queue
		let p = if let Some(p) = files.lock().unwrap().next() { p }
		else { return; };

		// open file
		let mut f = if let Ok(f) = File::open(&p) { f }
		else
		{
			warn!("Could not open {:?} as a file! Skipping...", &p);
			notifier.send(ProcessedMsg).unwrap();
			continue;
		};

		// get file extension
		let ext = p.extension().map(|s| s.to_string_lossy().into_owned());

		// start hashing
		// BLAKE3 is fastest with buffer size of 16KiB or higher, which allows it to use AVX512
		let hasher = Hasher::new();
		let mut writer = BufWriter::with_capacity(16384, hasher);
		let size = io::copy(&mut f, &mut writer).unwrap();
		let hash = writer.into_inner().unwrap().finalize();

		// add to map
		match map.get_mut(&hash)
		{
			Some(mut v) => v.value_mut().push((p, size, ext)),
			None => mem::drop(map.insert(hash, vec![(p, size, ext)]))
		}

		notifier.send(ProcessedMsg).unwrap();
	}
}
